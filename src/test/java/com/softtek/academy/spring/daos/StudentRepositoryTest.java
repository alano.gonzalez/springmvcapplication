package com.softtek.academy.spring.daos;



import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.spring.configuration.SpringMVCConfiguration;

import org.junit.Before; 

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { SpringMVCConfiguration.class })
@WebAppConfiguration
public class StudentRepositoryTest {
	
	@Qualifier("studentRepository")
	@Autowired
	StudentDAO studentDao;
	
	@Before
	public void init() {
	}
	
	@Test
	public void studentListTest() {
		assertNotNull("La lista no esta completa", studentDao.getAllStudents());
	}
	
}
