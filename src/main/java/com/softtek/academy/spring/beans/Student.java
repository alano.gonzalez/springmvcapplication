package com.softtek.academy.spring.beans;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student {
	
	private Integer age;
	private String name;
	@Id
	private Integer id;
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(Integer age, String name, Integer id) {
		super();
		this.age = age;
		this.name = name;
		this.id = id;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Student -> " + this.name + ", Age: " + this.age + ", id: " + this.id;
	}
	
	
	

}
