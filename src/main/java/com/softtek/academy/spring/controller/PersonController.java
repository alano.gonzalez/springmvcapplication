package com.softtek.academy.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.spring.beans.Student;
import com.softtek.academy.spring.service.StudentService;

@Controller
public class PersonController {
	 @Autowired
	 StudentService studentService;    
	
	@RequestMapping(value="/person", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person", "command", new Student());
	}
	
	
	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
    public String addPerson(@ModelAttribute("SpringWeb")Student person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        studentService.addStudent(person);
        model.addAttribute("command", new Student());
        return "person";
    }
	@RequestMapping(value="/viewPersons", method = RequestMethod.GET)
	 public String viewPersons(@ModelAttribute("SpringWeb")Student person, ModelMap model) {
		System.out.println(studentService.getAllStudents());
		model.addAttribute("students", studentService.getAllStudents());
		return "persons";
	}
	@RequestMapping(value="/alterPerson", method = RequestMethod.GET)
	 public String alterPerson(@ModelAttribute("SpringWeb")Student person, ModelMap model) {
		System.out.println(studentService.getAllStudents());
		model.addAttribute("students", studentService.getAllStudents());
		return "alterPerson";
	}
	@RequestMapping(value="/change/{id}")
    public String edit(@PathVariable int id, Model m){
		Student s = studentService.find(id);
        m.addAttribute("command", s);
        System.out.println(s);
        return "editStudent";
    }
	
	@RequestMapping(value = "/saveModify", method = RequestMethod.POST)
    public String modifyStud(@ModelAttribute("SpringWeb")Student person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        studentService.update(person);
       model.addAttribute("students", studentService.getAllStudents());
        return "alterPerson";
    }
	
	@RequestMapping(value="/deleteList", method = RequestMethod.GET)
	 public String deleteList(@ModelAttribute("SpringWeb")Student person, ModelMap model) {
		System.out.println(studentService.getAllStudents());
		model.addAttribute("students", studentService.getAllStudents());
		return "deleteList";
	}
	
	@RequestMapping(value="/delete/{id}")
    public String delete(@PathVariable int id, Model m){
		studentService.delete(id);
		m.addAttribute("students", studentService.getAllStudents());
        return "deleteList";
    }
	
}
