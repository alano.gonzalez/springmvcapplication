package com.softtek.academy.spring.service;

import java.util.List;

import com.softtek.academy.spring.beans.Student;

public interface StudentService {
	
	List<Student> getAllStudents();

	void addStudent(Student person);

	Student find(int id);

	void update(Student person);

	void delete(int id);

}
