package com.softtek.academy.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.spring.beans.Student;
import com.softtek.academy.spring.daos.StudentDAO;

@Transactional
@Service
public class StudentServiceImpl implements StudentService {
	
	@Qualifier("studentRepository")
	@Autowired
	StudentDAO studentDao;

	@Override
	public List<Student> getAllStudents() {
		return studentDao.getAllStudents();
	}

	@Override
	public void addStudent(Student person) {
		studentDao.addStudent(person);
	}

	@Override
	public Student find(int id) {
		return studentDao.find(id);
	}

	@Override
	public void update(Student person) {
		studentDao.update(person);
	}

	@Override
	public void delete(int id) {
		studentDao.delete(id);
	}


}
