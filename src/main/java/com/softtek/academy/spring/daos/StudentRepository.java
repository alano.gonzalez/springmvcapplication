package com.softtek.academy.spring.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.softtek.academy.spring.beans.Student;


@Repository("studentRepository")
public class StudentRepository implements StudentDAO{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	void setEM(EntityManager em){
		this.entityManager = em;
	}
	

	@Override
	public List<Student> getAllStudents() {
		Query query = entityManager.createQuery("select s from Student s", Student.class);
		return query.getResultList();
	}


	@Override
	public void addStudent(Student person) {
		entityManager.persist(person);
	}


	public Student find(int id) {
		return entityManager.find(Student.class, id);
	}


	@Override
	public void update(Student person) {
		entityManager.merge(person);
	}


	@Override
	public void delete(int id) {
		Student s = find(id);
		entityManager.remove(s);
	}

}
