package com.softtek.academy.spring.daos;

import java.util.List;

import com.softtek.academy.spring.beans.Student;

public interface StudentDAO {

	public List<Student> getAllStudents();

	public void addStudent(Student person);

	public Student find(int id);

	public void update(Student person);

	public void delete(int id);
}
